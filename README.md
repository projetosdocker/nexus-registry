# Configurando um servidor de laboratório Nexus3

Este repositório mostra como configurar um servidor Nexus para uso em seu laboratório doméstico.

## Build

Faça o Build da imagem;
```
docker build -t nexus-registry .
```

## persistência de dados
Crie o diretório de persistência de dados.
```
sudo mkdir -p /storage/deploy/nexus ; sudo chown -R 200 /storage/deploy/nexus
```

## Executando
para rodar como container padrão, execute com:
```
docker run -d \
-p8081:8081 -p8082:8082 -p8083:8083 \
--name nexus-registry \
-v /storage/deploy/nexus:/nexus-data \
-e INSTALL4J_ADD_VM_PARAMS="-Xms2703m -Xmx2703m -XX:MaxDirectMemorySize=2703m -Djava.util.prefs.userRoot=/some-other-dir" \
nexus-registry
```

para ver os logs, execute o comando:
```
docker logs -f nexus-registry
```
Estaremos esperando aparecer:
```
-------------------------------------------------

Started Sonatype Nexus OSS 3.xx.-xx

-------------------------------------------------
```
Assim que estiver rodando, acesse: http://SEU_IP:8081

## Primeiro login
Clique em 'Entrar'.

Você entrará utililzando o usuário admin, encontre a senha inicial do administrador executando:
```
docker exec -it nexus-registry cat /nexus-data/admin.password
```
No primeiro login, você será solicitado a alterar a senha do administrador, Se desejar, você pode habilitar o acesso anônimo.

## Criando redirect Docker hub

### Criando um blob de armazenamento
Para configurar um proxy para o Docker Hub:

- Clique no ícone de configuração (engrenagem).

- Criaremos um armazenamento de **blob Stores**
  - Navegue até 'Blob Stores'.
  - Crie um novo Blob Store do tipo File.
  - Você pode nomeá-lo como quiser, mas uma boa escolha é docker-hub.
  - Clique em 'Criar Blob Store'.

- Vá até Security > Realms
  - Adicione o **Docker Bearer Token Realm**
  - Clique em Save


### Criando proxy para o hub Docker

- Clique em **Repositories**
- Clique em **Criar repositories** e selecione **docker (proxy)**
- Dê algum nome ( docker-hub-proxy)
- Marque 'HTTP' e dê a ele uma porta válida (8082)
- Marque 'Permitir pull anônimo do docker'
- Em Proxy > Remote Storage, digite este URL:**https://registry-1.docker.io**
- Em Docker Index, selecione **Use Docker Hub**
- Em Armazenamento > Blob Store, selecione o armazenamento de blob que você criou anteriormente (docker-hub)
- Clique em **Criar repositório**


### Utilizando o Registry inseguro HTTP
Acesse o arquivo de configuração e insira as linhas abaixo:
`sudo vim /etc/docker/daemon.json`

```yaml
{
  "insecure-registries" : ["SEU_IP:8082",
          "SEU_IP:8083"]
}
```

Para aplicar, reinicie o serviço do Docker.

### Pull de imagem

Um exemplo de puxada:
```
docker pull SEU_IP:8082/ubuntu:22.04
```

- para verificar o pull da imagem no nexus, clique no quadrado próximo a engrenagem e a  caixa de pesquisa no menu superior.
- Cliquem em Browser
- Clique em **docker-hub-proxy**
- Cliquem nos diretórios v2-library-ubuntu-tags e você verá a tag da imagem.

## Registry privado

### Criando um blob de armazenamento
- Clique no ícone de configuração (engrenagem).
- Navegue até 'Blob Stores'.
- Crie um novo Blob Store do tipo File.
- Você pode nomeá-lo como quiser, mas uma boa escolha é **docker-private**.
- Clique em 'Criar Blob Store'.

### Criando o Registry

- Clique em **Repositories**
- Clique em **Create repository** e selecione **docker (hosted)**
- Dê algum nome (docker-private)
- Marque 'HTTP' e dê a ele uma porta válida (8083)
- Em Storage > Blob Store, selecione (docker-private)
- Clique em **Create Repository**

Certifique-se que adicionou a etapa **Utilizando o Registry inseguro HTTP** da configuração do docker.

## Login no Registry
Para se conectar ao repositório, você precisará fazer login usando o docker cli:
```
docker login SEU_IP:8083
```

## Pull de imagens

Após fazer o login, podemos fazer um build de nova imagem e fazer o envio com push.
Vamos fazer da seguinte forma:
- Baixar a imagem alpine
- Adicionar nova tag para ela
- fazer o push
```
docker pull alpine:3
docker tag alpine:3 SEU_IP:8083/alpine:3
docker push SEU_IP:8083/alpine:3
```

## Fonte
- https://github.com/chrisbmatthews/lab-nexus-sever#docker-hub-proxy
