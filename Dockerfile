FROM sonatype/nexus3:3.28.1

# A imagem precisa expor as portas abaixo:
# 8081: nexus web
# 8082: docker registry conector hub Docker
# 8083: docker registry privado
EXPOSE 8081 8082 8083
